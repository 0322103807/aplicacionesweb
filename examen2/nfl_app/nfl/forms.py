from django import forms
from .models import Ciudad, Equipo, Estadio

class CiudadForm(forms.ModelForm):
    class Meta:
        model = Ciudad
        fields = ["nombre"]
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre de la ciudad"}),
        }

class EquipoForm(forms.ModelForm):
    class Meta:
        model = Equipo
        fields = ["nombre", "ciudad"]
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre del equipo"}),
            "ciudad": forms.Select(attrs={"class": "form-control"}),
        }

class EstadioForm(forms.ModelForm):
    class Meta:
        model = Estadio
        fields = ["nombre", "equipo"]
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre del estadio"}),
            "equipo": forms.Select(attrs={"class": "form-control"}),
        }
