from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy
from .forms import CiudadForm, EquipoForm, EstadioForm
from .models import Ciudad, Equipo, Estadio

# Vistas para Ciudad
class CreateCiudad(CreateView):
    template_name = "nfl/create_ciudad.html"
    model = Ciudad
    form_class = CiudadForm
    success_url = reverse_lazy('nfl:list_ciudades')

class ListCiudades(ListView):
    template_name = "nfl/list_ciudades.html"  
    model = Ciudad
    context_object_name = 'ciudades'

class DetailCiudad(DetailView):
    template_name = "nfl/detail_ciudad.html"
    model = Ciudad
    context_object_name = 'ciudad'

class UpdateCiudad(UpdateView):
    template_name = "nfl/update_ciudad.html"
    model = Ciudad
    form_class = CiudadForm
    success_url = reverse_lazy('nfl:list_ciudades')

class DeleteCiudad(DeleteView):
    template_name = "nfl/delete_ciudad.html"
    model = Ciudad
    success_url = reverse_lazy('nfl:list_ciudades')

# Vistas para Equipo
class CreateEquipo(CreateView):
    template_name = "nfl/create_equipo.html"
    model = Equipo
    form_class = EquipoForm
    success_url = reverse_lazy('nfl:list_equipo')

class ListEquipos(ListView):
    template_name = "nfl/list_equipo.html"
    model = Equipo
    context_object_name = 'equipos'

class DetailEquipo(DetailView):
    template_name = "nfl/detail_equipo.html"
    model = Equipo
    context_object_name = 'equipo'

class UpdateEquipo(UpdateView):
    template_name = "nfl/update_equipo.html"
    model = Equipo
    form_class = EquipoForm
    success_url = reverse_lazy('nfl:list_equipo')

class DeleteEquipo(DeleteView):
    template_name = "nfl/delete_equipo.html"
    model = Equipo
    success_url = reverse_lazy('nfl:list_equipo')


# Vistas para Estadio
class CreateEstadio(CreateView):
    template_name = "nfl/create_estadio.html"
    model = Estadio
    form_class = EstadioForm
    success_url = reverse_lazy('nfl:list_estadio')

class ListEstadios(ListView):
    template_name = "nfl/list_estadio.html"  
    model = Estadio
    context_object_name = 'estadios'

class DetailEstadio(DetailView):
    template_name = "nfl/detail_estadio.html"
    model = Estadio
    context_object_name = 'estadio'

class UpdateEstadio(UpdateView):
    template_name = "nfl/update_estadio.html"
    model = Estadio
    form_class = EstadioForm
    success_url = reverse_lazy('nfl:list_estadio')

class DeleteEstadio(DeleteView):
    template_name = "nfl/delete_estadio.html"
    model = Estadio
    success_url = reverse_lazy('nfl:list_estadio')
