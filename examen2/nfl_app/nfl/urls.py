from django.urls import path
from . import views

app_name = 'nfl' 

urlpatterns = [
    # URLs para Ciudad
    path('ciudad/create/', views.CreateCiudad.as_view(), name='create_ciudad'),
    path('ciudad/list/', views.ListCiudades.as_view(), name='list_ciudades'),
    path('ciudad/<int:pk>/', views.DetailCiudad.as_view(), name='detail_ciudad'),
    path('ciudad/update/<int:pk>/', views.UpdateCiudad.as_view(), name='update_ciudad'),
    path('ciudad/<int:pk>/delete/', views.DeleteCiudad.as_view(), name='delete_ciudad'),

    # URLs para Equipo
    path('equipo/create/', views.CreateEquipo.as_view(), name='create_equipo'),
    path('equipo/list/', views.ListEquipos.as_view(), name='list_equipo'),
    path('equipo/<int:pk>/', views.DetailEquipo.as_view(), name='detail_equipo'),
    path('equipo/<int:pk>/update/', views.UpdateEquipo.as_view(), name='update_equipo'),
    path('equipo/<int:pk>/delete/', views.DeleteEquipo.as_view(), name='delete_equipo'),

    # URLs para Estadio
    path('estadio/create/', views.CreateEstadio.as_view(), name='create_estadio'),
    path('estadio/list/', views.ListEstadios.as_view(), name='list_estadio'),
    path('estadio/<int:pk>/', views.DetailEstadio.as_view(), name='detail_estadio'),
    path('estadio/<int:pk>/update/', views.UpdateEstadio.as_view(), name='update_estadio'),
    path('estadio/<int:pk>/delete/', views.DeleteEstadio.as_view(), name='delete_estadio'),
]
