from django.urls import path
from . import views

app_name = 'core' 

urlpatterns = [
    # URLs para Puesto
    path('puesto/create/', views.CreatePuesto.as_view(), name='create_puesto'),
    path('puesto/list/', views.ListPuesto.as_view(), name='list_puesto'),
    path('puesto/<int:pk>/', views.DetailPuesto.as_view(), name='detail_puesto'),
    path('puesto/update/<int:pk>/', views.UpdatePuesto.as_view(), name='update_puesto'),
    path('puesto/<int:pk>/delete/', views.DeletePuesto.as_view(), name='delete_puesto'),

    # URLs para Empleado
    path('empleado/create/', views.CreateEmpleado.as_view(), name='create_empleado'),
    path('empleado/list/', views.ListEmpleado.as_view(), name='list_empleado'),
    path('empleado/<int:pk>/', views.DetailEmpleado.as_view(), name='detail_empleado'),
    path('empleado/<int:pk>/update/', views.UpdateEmpleado.as_view(), name='update_empleado'),
    path('empleado/<int:pk>/delete/', views.DeleteEmpleado.as_view(), name='delete_empleado'),

    # URLs para Alergia
    path('alergia/create/', views.CreateAlergia.as_view(), name='create_alergia'),
    path('alergia/list/', views.ListAlergia.as_view(), name='list_alergia'),
    path('alergia/<int:pk>/', views.DetailAlergia.as_view(), name='detail_alergia'),
    path('alergia/<int:pk>/update/', views.UpdateAlergia.as_view(), name='update_alergia'),
    path('alergia/<int:pk>/delete/', views.DeleteAlergia.as_view(), name='delete_alergia'),
]
